# Introduction

This project creates a docker image with (open)jdk 10 and sbt 1.1 installed.

The image can be used to run sbt builds.

This repository is mirrored to https://gitlab.com/sw4j-net/jdk10-sbt1.1

## Deprecation

As Java 10 is deprecated this repository and the generated images will be removed in January 2019.
